#include <cmath>
#include <cassert>
#include <iostream>
#include <cstdlib>
using namespace std;

double computeSqrt(double param, double guess, double prec = 1e-6)
{
    double x = guess;
    bool stop = false;
    int iteration = 0;
    while (!stop && iteration < 101)
    {
        double tmp = x - (x*x -param)/(2.*x);
        stop = fabs(x-tmp) < prec;
        x = tmp;
        ++iteration;
    }
    return x;
}


int main(int argc, char *argv[])
{
   if (argc < 2)
   {
       cerr <<  "usage : "<<  argv[0] << " number " << endl;
   }
   double param = atof(argv[1]);
   double  sqrtApprox = computeSqrt(param, 1., 1e-10);
   double approx = sqrtApprox * sqrtApprox;
   cout << sqrtApprox << endl,
   assert(fabs(approx - param) < 1e-4);
}
