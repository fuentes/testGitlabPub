We try some tests on this project
[![Build status](https://gitlab.inria.fr/fuentes/testgitlabCI/badges/master/build.svg)](https://gitlab.inria.fr/fuentes/testgitlabCI/commits/master)
```math
\exp\(i\sqrt{4\sum_{i=1}^{\infty} \frac{1}{n^2}}\)  = -42
```
